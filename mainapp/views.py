from rest_framework.generics import ListAPIView, CreateAPIView, UpdateAPIView, DestroyAPIView
from .models import Todo
from .serializers import TodoSerializer, TodoListSerializer


class TodoListAPIView(ListAPIView):
    serializer_class = TodoListSerializer
    queryset = Todo.objects.all()


class TodoCreateAPIView(CreateAPIView):
    serializer_class = TodoSerializer
    queryset = Todo.objects.all()


class TodoUpdateAPIView(UpdateAPIView):
    serializer_class = TodoSerializer
    queryset = Todo.objects.all()
    lookup_field = 'id'


class TodoDeleteAPIView(DestroyAPIView):
    serializer_class = TodoSerializer
    queryset = Todo.objects.all()
    lookup_field = 'id'

