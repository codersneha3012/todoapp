from django.urls import path
from .views import TodoListAPIView, TodoCreateAPIView, TodoUpdateAPIView, TodoDeleteAPIView

urlpatterns = [
    path('todos', TodoListAPIView.as_view(), name='todo-list'),
    path('todos/add', TodoCreateAPIView.as_view(), name='todo-create'),
    path('todos/update/<int:id>', TodoUpdateAPIView.as_view(), name='todo-update'),
    path('todos/delete/<int:id>', TodoDeleteAPIView.as_view(), name='todo-delete')
]
