from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from .models import Todo
import json


class TodoTest(APITestCase):
    def test_todos(self):
        url = reverse('mainapp:todo-create')
        data = {'status': '2', 'due_date': '2018-10-10', 'text': 'Cleanup system'}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Todo.objects.count(), 1)
        self.assertEqual(Todo.objects.get(pk=1).text, 'Cleanup system')
        print(Todo.objects.all())

        url = reverse('mainapp:todo-list')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        url = reverse('mainapp:todo-update', args=[1])
        data = {'status': 2, 'due_date': '2018-10-13', 'text': 'Take backup'}
        response = self.client.patch(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        url = reverse('mainapp:todo-delete', args=[1])
        response = self.client.delete(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

        print(Todo.objects.all())
