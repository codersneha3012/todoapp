from rest_framework import serializers
from .models import Todo


class TodoListSerializer(serializers.ModelSerializer):
    status = serializers.SerializerMethodField()

    def get_status(self, obj):
        return obj.status_name

    class Meta:
        model = Todo
        fields = ('id', 'status', 'due_date', 'text')


class TodoSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Todo
        fields = ('id', 'status', 'due_date', 'text')