from django.db import models


class TodoStatus:
    TODO = 0
    IN_PROGRESS = 1
    DONE = 2

    STATUS_CHOICES = (
        (TODO, 'Todo'),
        (IN_PROGRESS, 'In Progress'),
        (DONE, 'Done'),
    )


class Todo(models.Model):
    status = models.IntegerField(choices=TodoStatus.STATUS_CHOICES)
    due_date = models.DateField()
    text = models.CharField(max_length=255)

    def __str__(self):
        return self.text

    @property
    def status_name(self):
        return dict(TodoStatus.STATUS_CHOICES).get(self.status)


