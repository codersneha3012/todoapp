# README #

### What is this repository for? ###

* This app can serve as backend to add todo things, update, delete them and fetch their list.

### Pre-requisites ###
* Python 3.x, pipenv

### How do I get set up? ###
Take clone and follow the steps below to get going.

* Install django 2.1 using command ```pipenv install django==2.1```
* Run ```pipenv install``` to install virtualenv and other dependencies.
* Run ```pipenv shell``` to start virtualenv.
* Makemigrations, migrate and runserver and your are ready to go.
Voila!! You have done.

### Running the tests ###
TestCases are written to test APIs to create, update, delete and list the TODOs.

* Run ```python3 manage.py test``` to run tests.